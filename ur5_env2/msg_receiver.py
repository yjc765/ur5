#!/usr/bin/env python3

import socket
from mujoco_py import load_model_from_path, MjSim, MjViewer


model = load_model_from_path("assets/ur5/peg_n_hole_ur5.xml")
sim = MjSim(model)

viewer = MjViewer(sim)

UDP_IP = "localhost"
UDP_PORT = 44432

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))


def ctr_set_joints(sim, joint_angles):
    """
    Directly set the joint_angles of the robot
    :param sim:
    :param joint_positions: dict of string joint ids (defined in the env xml) and the desired values
    :return:
    """
#    assert joint_angles is not None
    for k, v in joint_angles.items():
        sim.data.set_joint_qpos(k, v)


while True:
    data, addr = sock.recvfrom(1024)  # buffer size is 1024 bytes
    data = data.decode("utf-8")
    data = eval(data)
    print("received message:", data)

    joint_angles = {
        'robot0:joint1': data[1],
        'robot0:joint2': data[4],
        'robot0:joint3': data[0],
        'robot0:joint4': data[3],
        'robot0:joint5': data[5],
        'robot0:joint6': data[2],
    }
    ctr_set_joints(sim, joint_angles=joint_angles)

    t = 0
    while True:
        # sim.step()
        sim.forward()
        viewer.render()
        t += 1
        if t > 100:
            break