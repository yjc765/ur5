#!/usr/bin/env python3
"""
Displays robot fetch at a disco party.
"""
import os
from mujoco_py import load_model_from_path, MjSim, MjViewer
import numpy as np


model = load_model_from_path("assets/fetch/peg_n_hole_ur5.xml")
#model = load_model_from_path("assets/ur5/peg_n_hole_ur5.xml")
sim = MjSim(model)

viewer = MjViewer(sim)

t = 0


def ctrl_xyz(sim, x, y, z, a, b, c, d, is_delta):
    """
        For setting positions
        :param sim:
        :param positions and quaternions:
        :return:
        """
    action = np.array([x, y, z, a, b, c, d])
    action, _ = np.split(action, (sim.model.nmocap * 7,))
    action = action.reshape(sim.model.nmocap, 7)
    pos_delta = action[:, :3]
    quat_delta = action[:, 3:]

    if is_delta is True:
        sim.data.mocap_pos[:] = sim.data.mocap_pos + pos_delta
        sim.data.mocap_quat[:] = sim.data.mocap_quat + quat_delta
    else:
        sim.data.mocap_pos[:] = pos_delta
        sim.data.mocap_quat[:] = quat_delta



def ctr_set_joints(sim, joint_angles):
    """
    Directly set the joint_angles of the robot
    :param sim:
    :param joint_positions: dict of string joint ids (defined in the env xml) and the desired values
    :return:
    """
#    assert joint_angles is not None
    for k, v in joint_angles.items():
        sim.data.set_joint_qpos(k, v)


joint_angles = {
    'robot0:joint1': 0,
    'robot0:joint2': -1.57,
    'robot0:joint3': 1.57,
    'robot0:joint4': -1.57,
    'robot0:joint5': -1.57,
    'robot0:joint6': 1.57,
}


def draw_circle(size_point_cloud):
    if size_point_cloud == 1:
        # size => 379
        #total = 10
        total = 5
        step_size_loop = 0.5
    elif size_point_cloud == 2:
        # size => 1458
        total = 13
        step_size_loop = 0.1
    elif size_point_cloud == 3:
        # size => 1662
        total = 22
        step_size_loop = 0.1
    elif size_point_cloud == 4:
        # size => 1662
        total = 20
        step_size_loop = 0.03

    elif size_point_cloud == 5:
        # size => 460611
        total = 50
        step_size_loop = 0.007

    radius = 0.855
    shift_z = .0 #0.40

    lat = np.arange(0., np.pi+(np.pi/total), np.pi/total)
    lon = np.arange(0., 2*np.pi+(2*np.pi/total), 2*np.pi/total)

    #cicrle = []
    robot_trajectory = []
    x = []
    y = []
    z = []

    for i in range(0, total+1):
        for j in range(0, total+1):
            x = radius * np.sin(lat[i]) * np.cos(lon[j])
            y = radius * np.sin(lat[i]) * np.sin(lon[j])
            z = shift_z + radius * np.cos(lat[i])

            robot_trajectory.append([x, y, z])
        x_actual = radius * np.sin(lat[i]) * np.cos(lon[j])
        y_actual = radius * np.sin(lat[i]) * np.sin(lon[j])

        radius_circle = (x_actual*x_actual + y_actual*y_actual)**(0.5)
        for x_surface in np.arange(-radius_circle, radius_circle, step_size_loop):
            for y_surface in np.arange(-radius_circle, radius_circle, step_size_loop):
                radius_actual = (x_surface*x_surface + y_surface*y_surface)**(0.5)
                if radius_actual < radius_circle:
                    x = x_surface
                    y = y_surface
                    z = shift_z + radius * np.cos(lat[i])
                    robot_trajectory.append([x, y, z])
                else:
                    continue
    return robot_trajectory

#ctr_set_joints(sim, joint_angles=joint_angles)
#ctrl_xyz(sim, -0.3, 0.2, 1.5, 1, -1, 0, 0, is_delta=False)

positions = draw_circle(5)
quat = [1, -1., 0, 0.]
#print(len(positions))

joint_state = []
for position in positions:
    #print(position)
    ctrl_xyz(sim, position[0], position[1], position[2], quat[0], quat[1], quat[2], quat[3], is_delta=False)
    t = 0

    while True:
        sim.forward()
        sim.step()
        viewer.render()

        t += 1
        if t > 100:
            break
#    result = []
#    for i in range(1, 7):
#        result.append(sim.data.get_joint_qpos('robot0:joint' + str(i)))
    #print(result)
    print(sim.data.mocap_pos[:], sim.data.mocap_quat[:])
#    joint_state.append(result)


#joint_state = np.array(joint_state)
#print(joint_state.shape)
#np.savetxt('joint_states.txt', joint_state)

