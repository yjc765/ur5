#!/usr/bin/env python3
"""
Displays robot fetch at a disco party.
"""

import time
import socket


x = 0.4
y = [-0.4, -0.2, 0, 0.2, 0.4]
z = [0.1, 0.5]
while True:
    for n in z:
        for m in y:
            UDP_IP = 'localhost'
            UDP_PORT = 44433
            data = [x, m, n]
            MESSAGE = data.__str__()
            print("UDP target IP:", UDP_IP)
            print("UDP target port:", UDP_PORT)
            print("message:", MESSAGE)
            time.sleep(2)
            sock = socket.socket(socket.AF_INET, # Internet
                       socket.SOCK_DGRAM) # UDP
            sock.sendto(MESSAGE.encode(), (UDP_IP, UDP_PORT))


