#!/usr/bin/env python3
"""
Displays robot fetch at a disco party.
"""
from mujoco_py import load_model_from_path, MjSim, MjViewer
import os
import numpy as np
import time
import math

pi = math.pi
model = load_model_from_path("assets/ur5/peg_n_hole_ur5.xml")
sim = MjSim(model)

viewer = MjViewer(sim)


def ctrl_xyz(sim, x, y, z, a, b, c, d, is_delta):
    """
        For setting positions 
        :param sim:
        :param positions and quaternions:
        :return:
        """
    action = np.array([x, y, z, a, b, c, d])
    action, _ = np.split(action, (sim.model.nmocap * 7,))
    action = action.reshape(sim.model.nmocap, 7)
    pos_delta = action[:, :3]
    quat_delta = action[:, 3:]

    if is_delta is True:
        sim.data.mocap_pos[:] = sim.data.mocap_pos + pos_delta
        sim.data.mocap_quat[:] = sim.data.mocap_quat + quat_delta
    else:
        sim.data.mocap_pos[:] = pos_delta
        sim.data.mocap_quat[:] = quat_delta


# Controlling joints
def ctr_set_joints_action(sim, joint_actions):
    """
    For setting robot angles movement'delta_angle'
    :param sim:
    :param action: dict of string joint ids (defined in the env xml) and the desired values of movement
    :return:
    """
#    assert joint_actions is not None
    for k, v in joint_actions.items():
        v_new = v + sim.data.get_joint_qpos(k)
        sim.data.set_joint_qpos(k, v_new)


def ctr_set_joints(sim, joint_angles):
    """
    Directly set the joint_angles of the robot
    :param sim:
    :param joint_positions: dict of string joint ids (defined in the env xml) and the desired values
    :return:
    """
#    assert joint_angles is not None
    for k, v in joint_angles.items():
        sim.data.set_joint_qpos(k, v)


joint_states = [-2.964906853246579317e+02, -5.748308352975282105e+02, 3.810611027115193110e+02, 1.951905463728982681e+02, 1.244889892016907806e+00, -2.980299463033397274e+02]
joint_angles = {
    'robot0:joint1': 1.57,
    'robot0:joint2': -1.57,
    'robot0:joint3': 1.57,
    'robot0:joint4': -1.57,
    'robot0:joint5': -1.57,
    'robot0:joint6': 0,
}
joint_actions = {
    'robot0:joint1': 0,
    'robot0:joint2': 0,
    'robot0:joint3': 0.005,
    'robot0:joint4': 0,
    'robot0:joint5': 0,
    'robot0:joint6': 0,
}


#ctr_set_joints(sim, joint_angles=joint_angles)

ctrl_xyz(sim, 0, 0.3, 0.5, 1, -1, 0, 0, is_delta=False)
a = 1
t = 0
while True:

    result = []
    for i in range(1, 7):
        result.append(sim.data.get_joint_qpos('robot0:joint' + str(i)))
    print(result)

#    ctr_set_joints_action(sim, joint_actions)

    sim.step()
    sim.forward()
    viewer.render()

    if a == 1:
        time.sleep(2)
        a += 1
    else:
        time.sleep(0.01)
    t += 1
    if t > 100 and os.getenv('TESTING') is not None:
        break

