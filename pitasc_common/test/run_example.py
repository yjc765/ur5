#!/usr/bin/env python

import unittest
import rostest

# python
import argparse
import os
import sys
import time

# ros
import rospy
import rospkg
import tf2_ros

# pitasc
from pitasc.model import Model
from pitasc.executor import Executor

class Example_Test(unittest.TestCase):

    def test_example(self):
        if os.environ.get('PUBLICBUILD') is None:
            retval = self.exec_example()
            self.assertEqual(retval, 0, "Test did not run successfully!")
        else:
            with self.assertRaises(SystemExit, msg="Test was run, but should have been aborted by license check!"):
                self.exec_example()

    def exec_example(self):

        path = rospy.get_param("path")
        file = rospy.get_param("file")
        app = rospy.get_param("app", '')

        # ROS
        rospy.init_node('pitasc', log_level=rospy.INFO)

        # Wait for transformation
        buf = tf2_ros.Buffer()
        lis = tf2_ros.TransformListener(buf)
        while(not buf.can_transform("target3", "world", rospy.Time())):
            print "Waiting for transformation from base_link to target3"
            time.sleep(1.0)

        # Some more waiting to be sure
        time.sleep(2.0)

        # Load file
        model = Model()
        file_name = os.path.join(path, file)
        model.import_file(file_name)

        # Execute
        executor = Executor(model)
        with executor.stop_on_sigint():
            if isinstance(app, str):
                executor.start(app, [])
            elif isinstance(app, list):
                for each_app in app:
                    executor.start(each_app, [])

        return 0

if __name__ == '__main__':
    rostest.rosrun('pitasc_common', 'test_example', Example_Test)

# eof
