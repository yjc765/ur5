#!/usr/bin/env python

import rospkg

# python
import argparse
import glob, os
import sys

from lxml import etree

def main():
    parser = argparse.ArgumentParser(description='This script creates rostests for pitasc with for each .xml file within the given folder.')
    parser.add_argument('rospackage',
                        help='the name of the rospackage')
    parser.add_argument('path',
                        help='path within the rospackage to the folder to be parsed.')
    #parser.add_argument('output', default='/',
    #                    help='path to the folder to be output the rostest.')

    args = parser.parse_args()

    rospack = rospkg.RosPack()
    path = os.path.join(rospack.get_path(args.rospackage), args.path)

    for file in os.listdir(path):
        if file.endswith(".xml"):
            # print(file)
            launch = etree.Element("launch")

            etree.SubElement(launch, "param", name="path", value=os.path.join("$(find {})".format(args.rospackage), args.path))
            etree.SubElement(launch, "param", name="file", value="{}".format(file))

            incl = etree.SubElement(launch, "include", file="$(find pitasc_common)/launch/bringup_examples.launch")
            etree.SubElement(incl, "arg", name="gui", value="False")

            etree.SubElement(launch, "test", testname="Test_{}".format(os.path.splitext(file)[0]), pkg="pitasc_common", type="run_example.py", timelimit="300.0")

            tree = etree.ElementTree(launch)

            xml_string = etree.tostring(tree, encoding='utf-8', xml_declaration=True, pretty_print=True)

            xml_string = xml_string.replace('testname', 'test-name')
            xml_string= xml_string.replace('timelimit', 'time-limit')

            with open("{}_example.test".format(os.path.splitext(file)[0]),"w+") as f:
                f.write(xml_string)




if __name__ == '__main__':
    main()

# eof
