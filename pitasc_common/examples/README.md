
## Run the demos:

```roslaunch pitasc_common bringup_examples.launch```

For the dual robot arm demos, please launch this one instead

```roslaunch pitasc_common bringup_dual_arm_examples.launch```

Run an application

```rosrun pitasc file_reader.py pitasc_common examples/<folder>/<app_name>```

**folder**: e.g. 'beginner'

**app_name**: one of the following xml files

## Examples overview

### Beginner

* **simple_idle.xml** Idles for 3 seconds and then ends
* **simple_move.xml** Moves to a target (LIN)
* **skill_sequence.xml** Performs a set of motion skills in a sequence
* **skill_group.xml** Performs a set of motion skills in a statemachine (group)
* **skill_hierarchy.xml** Performs a set of motion skills at the same time (!) which are in a hierarchy (prioritized)
* **watchdogs.xml** Shows how to add watchdogs to end skills on certain events (e.g. timeout)
* **forces.xml** Shows how to create force-based assembly motions

### Dual robot

* **simple_dual_robot.xml** A simple demo with a UR5 and a LBR4

### Skills

* **cylidrical_helix.xml** Performs a rotational and linear forward motion at the same time, resulting in a "screwdriver" motion
* **conical_helix.xml** Performs a rotational motion around a pivoint point and a linear forward motion at the same time, resulting in a conical-helix motion
* **circular_push.xml** Performs a circular push motion (force-based)
