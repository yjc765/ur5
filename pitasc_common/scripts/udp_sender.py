#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
import socket
import time

UDP_IP = 'localhost'
UDP_PORT = 44432


def callback(data):
    rospy.loginfo(data.position)

    MESSAGE = data.position.__str__()
#    print("UDP target IP:", UDP_IP)
#    print("UDP target port:", UDP_PORT)
#    print("message:", MESSAGE)
    sock = socket.socket(socket.AF_INET,  # Internet
                         socket.SOCK_DGRAM)  # UDP
    sock.sendto(MESSAGE.encode(), (UDP_IP, UDP_PORT))


def main():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('/joint_states', JointState, callback)
    rospy.spin()


if __name__ == '__main__':
    main()



