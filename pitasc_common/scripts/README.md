README.md
# Integrating Pitasc and Mujoco
Integrating Pitasc + RViz with Mujoco (UR5 environment)

### Entities
1. `ur5.py`
    1. Open UDP connection on port 44433
    2. Blocking/asynchronous send array of [x,y,z]
    3. Receive joint position over UDP
2. `moving_publisher.py`
    1. Open UDP connection on port 44433
    2. Receive array x,y,z
    3. publish as ros topic to RVIZ
    4. subscribe to joint position from RVIZ
    5. Receive joint angles from RVIZ
    6. Send joint angles over UDP to ur5.py
3. `pitask`
    1. Run track_frame

