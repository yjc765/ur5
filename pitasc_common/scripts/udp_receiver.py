import socket
import rospy
from visualization_msgs.msg import InteractiveMarkerFeedback

UDP_IP = "localhost"
UDP_PORT = 44433

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

def talker():
    pub = rospy.Publisher('/interactive_marker_server/feedback', InteractiveMarkerFeedback, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    imf = InteractiveMarkerFeedback()
    #imf.header.stamp = 'now'
    imf.header.frame_id = 'base_link'
    imf.client_id = '/rviz/InteractiveMarkers'
    imf.marker_name = '/interactive_transform_publisher'
    imf.control_name = 'move_y'
    #imf.event_type = 0
    imf.pose.orientation.x = 1
    imf.pose.orientation.y = 0
    imf.pose.orientation.z = 0
    imf.pose.orientation.w = 0
    #imf.menu_entry_id = 0
    #imf.mouse_point = [0,0,0]
    #imf.mouse_point_valid = False

    while True:
        data, addr = sock.recvfrom(1024)  # buffer size is 1024 bytes
        data = data.decode("utf-8")
        data = eval(data)
        x = data[0]
        y = data[1]
        z = data[2]
        print(type(data))
        print("received message:", data)

        #rate = rospy.Rate(0.5)
        imf.pose.position.x = x
        imf.pose.position.y = y
        imf.pose.position.z = z
        rospy.loginfo('Publish x: {}, y: {}, z:{}'.format(x, y, z))
        pub.publish(imf)
        #rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass



